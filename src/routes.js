import React from "react";
import { Route, Switch } from "react-router-dom";
import Search from "./views/search";
import Weather from "./views/weather";
import NotFound from "./views/notFound";
import AppliedRoute from "./components/appliedRoute";

export default ({ childProps }) =>
    <Switch>
        <AppliedRoute path="/" exact component={Search} props={childProps} />
        <AppliedRoute path="/weather" exact component={Weather} props={childProps} />
        <Route component={NotFound} />
    </Switch>;